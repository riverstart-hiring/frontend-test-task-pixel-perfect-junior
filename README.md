# Верстка

Тестовое задание на позицию `frontend` разработчика.

## Задача

Сверстать предоставленный [макет](https://www.figma.com/file/AhMuwGpK7HFx4Mg9W3syHS/%D0%92%D0%B5%D1%80%D1%81%D1%82%D0%BA%D0%B0?node-id=0%3A1). 

## Требования

- Полный список требований в работе собран и описан в [репозитории](https://gitlab.com/riverstart-hiring/frontend/frontend-guide)
- Дополнительно к нему:
  - Кроссбраузерность (последние 2 версии chrome, firefox, safari)
  - Придерживаться в верстке принципов pixel perfect (отклонения от макет допускаются, но минимальные)
  - Анимации и состояния: ховеры у кнопок, плавное заполнение шкалы процентов при инициализации, подсветка заголовков табов

### Дополнительно (будет преимуществом)

- Выполнение комментариев отмеченных звездочкой (*) в фигме

- Минимальный адаптив с сохранением контента в области видимости без наличия горизонтального скрола

## Решение

1. Сделать форк данного репозитория
2. Склонировать репозиторий со стартовой сборкой (см. гайд)
3. Реализовать код
4. Создать merge request в данный репозиторий
5. Предоставить ссылку на репозиторий с решением
6. Репозиторий должен быть публичным
7. В проекте необходимо дополнить `readme` с инструкцией по запуску приложения.


Удачи! <3
